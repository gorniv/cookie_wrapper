// conditional import for web and non-web
import 'package:flutter/foundation.dart';

import 'cookie_web_stubs.dart' if (dart.library.html) 'cookie_web.dart';

abstract class Cookie {
  factory Cookie.create() {
    if (kIsWeb) {
      return CookieHandlerWeb();
    } else {
      return CookieHandlerStubs();
    }
  }

  String? get(String key);

  bool remove(String key, {String? path, String? domain, bool? secure});

  void set(
    String key,
    String value, {
    DateTime? expires,
    Duration? maxAge,
    String? path,
    String? domain,
    bool? secure,
  });

  String formatDate(DateTime date);
}

class CookieHandlerStubs implements Cookie {
  // Implement methods for MacOS

  @override
  String? get(String key) {
    return null;
  }

  @override
  void set(
    String key,
    String value, {
    DateTime? expires,
    Duration? maxAge,
    String? path,
    String? domain,
    bool? secure,
  }) {
    // no op
  }

  @override
  bool remove(String key, {String? path, String? domain, bool? secure}) {
    // no op
    return false;
  }

  @override
  String formatDate(DateTime date) {
    // no op
    return '';
  }
}
