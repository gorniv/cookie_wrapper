# Cookie Wrapper



An [HTTP Cookies](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies) implementation for the client.

Forked from the [Cooky](https://pub.dev/packages/cooky) package to support cross-platform use cases (for speed of development). If you don't need to be able to compile against anything other than web then please use that.

## Usage

```dart
import 'package:cookie_wrapper/cookie.dart';

var cookie = Cookie.create();

setCookie() async {
  cookie.set('key', 'value'); // returns empty for non-web
}
```

See our [example/example.dart](example/example.dart) for a more complete example. 